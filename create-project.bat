REM Definindo o tamanho da janela CMD
mode con:cols=90 lines=40

REM Desligando as mensagens nativas do PROMPT
@echo off

REM Titulo da caixa de mensagem
title Create project with minFrame - first version (.bat)
echo _________________________________________________________________________________________
echo #########################################################################################
echo #                               @author: fabio campello                                 #
echo #                                    created in 2019                                    #
echo #                                     version:0.1.0                                     #
echo #                                An project open source                                 #
echo #########################################################################################

REM Entrada de dados de configuração inicial do projeto
echo Let's create our web project. 
set /p "nameProject=Input name project:"
set /p "descritionProject=Input descrition project:"
set /p "author=Input author from project:"

REM Selecionando diretório para a instalação do projeto
set "psCommand="(new-object -COM 'Shell.Application')^.BrowseForFolder(0,'Please choose a folder.',0,0).self.path""
for /f "usebackq delims=" %%I in (`powershell %psCommand%`) do set "folder=%%I"
cd /d %folder%
mkdir %nameProject%
cd %nameProject%

echo.
echo 01 - Creating file structure...

REM Criando subpastas
mkdir src
mkdir assets 
cd assets
mkdir css
mkdir images
mkdir js

REM Criando o arquivo principal de estilo - main.css
echo 02 - Creating file main.css...

cd css
echo * { > main.css
echo     box-sizing: border-box; >> main.css
echo } >> main.css
echo. >> main.css
echo @media screen and (min-width: 1200px) and (max-width: 900px) { >> main.css
echo     .card { >> main.css
echo         width: 50%%; >> main.css
echo     } >> main.css
echo } >> main.css
echo. >> main.css
echo @media screen and (min-width: 600px) and (max-width: 900px) { >> main.css
echo     .card { >> main.css
echo         width: 50%%; >> main.css
echo     } >> main.css
echo } >> main.css
echo. >> main.css
echo @media screen and (max-width: 600px) and (max-width: 480px) { >> main.css
echo     .card { >> main.css
echo       width: 90%%; >> main.css
echo     } >> main.css
echo } >> main.css
echo. >> main.css
echo html, >> main.css
echo body { >> main.css
echo     color: #444; >> main.css
echo     font-family: 'Helvetica', 'Verdana', sans-serif; >> main.css
echo     -moz-osx-font-smoothing: grayscale; >> main.css
echo     -webkit-font-smoothing: antialiased; >> main.css
echo     height: 100%%; >> main.css
echo     margin: 0; >> main.css
echo     padding: 0; >> main.css
echo     width: 100%%; >> main.css
echo } >> main.css
echo. >> main.css
echo html { >> main.css
echo     overflow: hidden; >> main.css
echo } >> main.css
echo. >> main.css
echo body { >> main.css
echo     align-content: stretch; >> main.css
echo     align-items: stretch; >> main.css
echo     background: #6C7A89; >> main.css
echo     display: flex; >> main.css
echo     flex-direction: column; >> main.css
echo     flex-wrap: nowrap; >> main.css
echo     justify-content: flex-start; >> main.css
echo } >> main.css
echo. >> main.css
echo .main { >> main.css
echo     flex: 1; >> main.css
echo     overflow-x: hidden; >> main.css
echo     overflow-y: auto; >> main.css
echo     padding-top: 60px; >> main.css
echo } >> main.css
echo. >> main.css
echo .header { >> main.css
echo     align-content: center; >> main.css
echo     align-items: stretch; >> main.css
echo     background: #3f51b5; >> main.css
echo     box-shadow: >> main.css
echo         0 4px 5px 0 rgba(0, 0, 0, 0.14), >> main.css
echo         0 2px 9px 1px rgba(0, 0, 0, 0.12), >> main.css
echo         0 4px 2px -2px rgba(0, 0, 0, 0.2); >> main.css
echo     color: #fff; >> main.css
echo     display: flex; >> main.css
echo     flex-direction: row; >> main.css
echo     flex-wrap: nowrap; >> main.css
echo     font-size: 20px; >> main.css
echo     height: 56px; >> main.css
echo     justify-content: flex-start; >> main.css
echo     padding: 16px 16px 0 16px; >> main.css
echo     position: fixed; >> main.css
echo     transition: transform 0.233s cubic-bezier(0, 0, 0.21, 1) 0.1s; >> main.css
echo     width: 100%%; >> main.css
echo     will-change: transform; >> main.css
echo     z-index: 1000; >> main.css
echo } >> main.css
echo. >> main.css
echo .header h1 { >> main.css
echo     flex: 1; >> main.css
echo     font-size: 20px; >> main.css
echo     font-weight: 400; >> main.css
echo     margin: 0; >> main.css
echo } >> main.css
echo. >> main.css
echo .header .powered-by { >> main.css
echo     color: white; >> main.css
echo     font-size: 0.6em; >> main.css
echo     text-decoration: none; >> main.css
echo } >> main.css
echo. >> main.css
echo .msg-frame { >> main.css
echo     margin-top: 10%%; >> main.css
echo     color: #fff; >> main.css
echo } >> main.css
echo. >> main.css
echo .detais { >> main.css
echo     position: fixed; >> main.css
echo     float: right; >> main.css
echo     bottom: 5%%; >> main.css
echo } >> main.css
echo. >> main.css
echo .spotlight { >> main.css
echo     color: blue; >> main.css
echo     text-decoration: unset; >> main.css
echo } >> main.css
echo. >> main.css
echo .card { >> main.css
echo     background: #fff; >> main.css
echo     border-radius: 2px; >> main.css
echo     box-shadow: >> main.css
echo       0 2px 2px 0 rgba(0, 0, 0, 0.14), >> main.css
echo       0 3px 1px -2px rgba(0, 0, 0, 0.2), >> main.css
echo       0 1px 5px 0 rgba(0, 0, 0, 0.12); >> main.css
echo     box-sizing: border-box; >> main.css
echo     margin: 16px; >> main.css
echo     padding: 16px; >> main.css
echo } >> main.css
echo. >> main.css
echo .container { >> main.css
echo     width: 100vw; >> main.css
echo     height: 100vh; >> main.css
echo     background: #6C7A89; >> main.css
echo     display: flex; >> main.css
echo     flex-direction: row; >> main.css
echo     justify-content: center; >> main.css
echo     align-items: center >> main.css
echo } >> main.css
echo. >> main.css
echo .box { >> main.css
echo     width: 300px; >> main.css
echo     background: #fff; >> main.css
echo } >> main.css
echo. >> main.css
echo .offline { >> main.css
echo     display: block; >> main.css
echo     margin-bottom: 1.5em; >> main.css
echo     margin-left: auto; >> main.css
echo     margin-right: auto; >> main.css
echo     width: 50%% >> main.css;
echo } >> main.css
echo. >> main.css
echo .margin-0 ^> div { >> main.css
echo     margin: 0; >> main.css
echo } >> main.css

cd..
cd..

REM Criando a pagina html inicial 
echo 03 - Creating files .html...

echo ^<!DOCTYPE html^> > index.html 
echo ^<html lang="en"^> >> index.html 
echo     ^<head^> >> index.html 
echo         ^<meta charset="UTF-8"^> >> index.html 
echo         ^<meta name="viewport" content="width=device-width, initial-scale=1.0"^> >> index.html 
echo         ^<link rel="stylesheet" href="./assets/css/main.css"^> >> index.html
echo          ^<link rel="icon" type="image/png" sizes="32x32" href="./assets/images/favicon-32x32.png"^> >> index.html
echo          ^<link rel="icon" type="image/png" sizes="16x16" href="./assets/images/favicon-16x16.png"^> >> index.html
echo          ^<link rel="manifest" href="./manifest.json"^> >> index.html
echo. >> index.html
echo          ^<!-- Add iOS meta tags and icons --^> >> index.html
echo          ^<meta name="apple-mobile-web-app-capable" content="yes"^> >> index.html
echo          ^<meta name="apple-mobile-web-app-status-bar-style" content="black"^> >> index.html
echo          ^<meta name="apple-mobile-web-app-title" content="minFrame"^> >> index.html
echo          ^<link rel="apple-touch-icon" href="/images/icons/icon-152x152.png"^> >> index.html
echo. >> index.html
echo          ^<!-- Add description here --^> >> index.html
echo          ^<meta name="description" content="A project with minFrame"^> >> index.html
echo. >> index.html
echo          ^<!-- Add meta theme-color --^> >> index.html
echo          ^<meta name="theme-color" content="#2F3BA2" /^> >> index.html
echo. >> index.html
echo         ^<title^>minFrame^</title^> >> index.html 
echo     ^</head^> >> index.html 
echo. >> index.html
echo     ^<body^> >> index.html 
echo          ^<header class="header"^> >> index.html 
echo               ^<h1^>minFrame^<a href="#" class="powered-by"^>^&nbsp;^&nbsp;FRAMEWORK^</a^>^</h1^> >> index.html 
echo          ^</header^> >> index.html 
echo. >> index.html
echo         ^<center class="msg-frame main"^> >> index.html
echo             ^<h2^> >> index.html
echo                 ^<span^>Welcome to the ^<a class="spotlight"^>minframe^</a^> web framework.^</span^>^<br^> >> index.html 
echo                 ^<span^>In its technology stack we have basically: ^</span^>^<br^> >> index.html
echo                 ^<span^>HTML, CSS, javascript, webpack and PWA.^</span^>^<br^> >> index.html
echo             ^</h2^> >> index.html 
echo         ^</center^> >> index.html
echo. >> index.html
echo         ^<div class="detais card"^> >> index.html
echo             ^<h3^>Contact of the author^</h3^> >> index.html
echo             ^<span^>^<b^>@author:^</b^> Campello, Fabio F.^</span^>^<br^> >> index.html
echo             ^<span^>^<b^>@email:^</b^> ^<a href="javascript:void(0)"^>fabiocampellodeveloper@gmail.com^</a^>^</span^>^<br^> >> index.html
echo             ^<span^>^<b^>@created in:^</b^> August 30, 2019^</span^>^<br^> >> index.html
echo             ^<span^>^<b^>@version:^<^/b^> 0.1.0^</span^>^<br^> >> index.html
echo         ^</div^> >> index.html 
echo. >> index.html
echo     ^</body^> >> index.html
echo. >> index.html
echo     ^<script^> >> index.html
echo         // Register service worker. >> index.html
echo         if ('serviceWorker' in navigator) { >> index.html
echo             window.addEventListener('load', () =^> { >> index.html
echo                 navigator.serviceWorker.register('/service-worker.js') >> index.html
echo                     .then((reg) =^> { >> index.html
echo                         console.log('Service worker registered.', reg); >> index.html
echo                     }); >> index.html
echo             }); >> index.html
echo         } >> index.html
echo     ^</script^> >> index.html
echo. >> index.html
echo ^</html^> >> index.html

REM Criando a página offline.html

echo ^<!DOCTYPE html^> > offline.html
echo ^<html lang="en"^> >> offline.html
echo. >> offline.html
echo ^<head^> >> offline.html
echo      ^<meta charset="UTF-8"^> >> offline.html
echo      ^<meta name="viewport" content="width=device-width, initial-scale=1.0"^> >> offline.html
echo      ^<link rel="stylesheet" href="./assets/css/main.css"^>  >> offline.html
echo      ^<link rel="icon" type="image/png" sizes="32x32" href="./assets/images/favicon-32x32.png"^> >> offline.html
echo      ^<link rel="icon" type="image/png" sizes="16x16" href="./assets/images/favicon-16x16.png"^> >> offline.html
echo      ^<link rel="manifest" href="/manifest.json"^> >> offline.html
echo. >> offline.html
echo      ^<!-- Add description here --^> >> offline.html
echo      ^<meta name="description" content="A project with minFrame"^> >> offline.html
echo. >> offline.html 
echo      ^<!-- Add meta theme-color --^> >> offline.html
echo      ^<meta name="theme-color" content="#2F3BA2" /^> >> offline.html
echo. >> offline.html
echo      ^<title^>minFrame^</title^> >> offline.html
echo ^</head^> >> offline.html
echo. >> offline.html
echo ^<body^> >> offline.html
echo. >> offline.html
echo      ^<header class="header"^> >> offline.html
echo           ^<h1^>minFrame^<a href="#" class="powered-by"^>^&nbsp;^&nbsp;FRAMEWORK^</a^>^</h1^> >> offline.html
echo     ^</header^> >> offline.html
echo     ^<main class="main container"^> >> offline.html
echo. >> offline.html
echo          ^<div class="card box"^> >> offline.html
echo               ^<img src="./assets/images/offline.png" alt="" class="offline"^> >> offline.html
echo               ^<div^> >> offline.html
echo                     Oops, you appear to be offline, this app requires an internet >> offline.html
echo                     connection. >> offline.html
echo               ^</div^> >> offline.html
echo          ^</div^> >> offline.html
echo. >> offline.html
echo   ^</main^> >> offline.html
echo ^</body^> >> offline.html
echo ^</html^> >> offline.html

REM Criando arquivo .js - ponto de entrada
cd src
type NUL > index.js
cd..

REM Criando arquivo manifest.json
echo 04 - Creating files manifest.json, service-worker.js, package.json...

echo { > manifest.json
echo     "name": "An project with minFrame", >> manifest.json
echo     "short_name": "AJYA", >> manifest.json
echo     "theme_color": "#005f97", >> manifest.json
echo     "background_color": "#005f97", >> manifest.json
echo     "display": "standalone", >> manifest.json
echo     "scope": "/", >> manifest.json
echo     "start_url": "/index.html", >> manifest.json
echo     "lang": "pt-BR", >> manifest.json
echo     "orientation": "any", >> manifest.json
echo     "icons": [ >> manifest.json
echo       { >> manifest.json
echo         "src": "/assets/images/favicon-512x512.png", >> manifest.json
echo         "sizes": "512x512", >> manifest.json
echo         "type": "image/png" >> manifest.json
echo       } >> manifest.json
echo     ] >> manifest.json
echo } >> manifest.json

REM Criando arquivo service-worker.js

echo 'use strict'; > service-worker.js
echo. >> service-worker.js
echo const CACHE_NAME = 'static-cache-v1'; >> service-worker.js
echo. >> service-worker.js
echo // Update cache names any time any of the cached files change. >> service-worker.js
echo const FILES_TO_CACHE = [ >> service-worker.js
echo. >> service-worker.js
echo     // style files >> service-worker.js
echo     '/assets/css/main.css', >> service-worker.js
echo. >> service-worker.js
echo     // images files >> service-worker.js
echo     './assets/images/offline.png', >> service-worker.js
echo. >> service-worker.js 
echo     //html files >> service-worker.js
echo     '/index.html', >> service-worker.js
echo     '/offline.html' >> service-worker.js
echo. >> service-worker.js
echo ]; >> service-worker.js
echo. >> service-worker.js
echo self.addEventListener('install', (evt) =^> { >> service-worker.js
echo     console.log('[ServiceWorker] Install'); >> service-worker.js
echo     // Precache static resources here. >> service-worker.js
echo     evt.waitUntil( >> service-worker.js
echo         caches.open(CACHE_NAME).then((cache) =^> { >> service-worker.js
echo             console.log('[ServiceWorker] Pre-caching offline page'); >> service-worker.js
echo             return cache.addAll(FILES_TO_CACHE); >> service-worker.js
echo         }) >> service-worker.js
echo     ); >> service-worker.js
echo. >> service-worker.js
echo     self.skipWaiting(); >> service-worker.js
echo }); >> service-worker.js
echo. >> service-worker.js
echo self.addEventListener('activate', (evt) =^> { >> service-worker.js
echo     console.log('[ServiceWorker] Activate'); >> service-worker.js
echo     // Remove previous cached data from disk. >> service-worker.js
echo     self.clients.claim(); >> service-worker.js
echo }); >> service-worker.js
echo. >> service-worker.js
echo // Serve from Cache >> service-worker.js
echo this.addEventListener("fetch", event =^> { >> service-worker.js
echo     event.respondWith( >> service-worker.js
echo       caches.match(event.request) >> service-worker.js
echo         .then(response =^> { >> service-worker.js
echo           return response ^|^| fetch(event.request); >> service-worker.js
echo         }) >> service-worker.js
echo         .catch(() =^> { >> service-worker.js
echo           return caches.match('/offline.html'); >> service-worker.js
echo         }) >> service-worker.js
echo     ) >> service-worker.js
echo }); >> service-worker.js

REM Criando o package.json

echo { > package.json 
echo   "name": "%nameProject%", >> package.json
echo   "version": "0.0.1", >> package.json
echo   "description": "%descritionProject%", >> package.json
echo   "main": "index.js", >> package.json
echo   "scripts": { >> package.json
echo     "test": "echo \"Error: no test specified\" && exit 1", >> package.json
echo     "watch": "webpack --watch", >> package.json
echo     "start": "webpack-dev-server --mode development --open", >> package.json
echo     "build": "webpack --mode production" >> package.json
echo   }, >> package.json
echo   "repository": { >> package.json
echo     "type": "", >> package.json
echo     "url": "" >> package.json
echo   }, "keywords": [], >> package.json
echo   "author": "%author%", >> package.json
echo   "license": "MIT" >> package.json
echo } >> package.json

REM Criando wabpack.config.js
echo 05 - Creating file wabpack.config.js...

echo const path = require('path'); >> wabpack.config.js
echo module.exports = { >> wabpack.config.js
echo     module: 'production', >> wabpack.config.js    
echo     entry: path.resolve(__dirname, './index.js'), >> wabpack.config.js
echo     output: { >> wabpack.config.js
echo         path: path.resolve(__dirname, 'dist'), >> wabpack.config.js
echo         filename: 'bundle.js', >> wabpack.config.js
echo         publicPath: '/' >> wabpack.config.js
echo     } >> wabpack.config.js
echo } >> wabpack.config.js

REM Criando a pagina html de test do Jasmine 

echo ^<!DOCTYPE html^> > SpecRunner.html   
echo ^<html lang="en"^> >> SpecRunner.html   
echo     ^<head^> >> SpecRunner.html  
echo         ^<meta charset="UTF-8"^> >> SpecRunner.html  
echo         ^<meta name="viewport" content="width=device-width, initial-scale=1.0"^> >> SpecRunner.html  
echo         ^<link rel="stylesheet" href="./assets/css/main.css"^> >> SpecRunner.html 
echo         ^<link rel="shortcut icon" type="image/png" href="./node_modules/jasmine-core/images/jasmine_favicon.png"^> >> SpecRunner.html
echo         ^<link rel="stylesheet" type="text/css" href="./node_modules/jasmine-core/lib/jasmine-core/jasmine.css"^> >> SpecRunner.html
echo. >> SpecRunner.html
echo         ^<script type="text/javascript" src="./node_modules/jasmine-core/lib/jasmine-core/jasmine.js"^>^</script^> >> SpecRunner.html
echo         ^<script type="text/javascript" src="./node_modules/jasmine-core/lib/jasmine-core/jasmine-html.js"^>^</script^> >> SpecRunner.html
echo         ^<script type="text/javascript" src="./node_modules/jasmine-core/lib/jasmine-core/boot.js"^>^</script^> >> SpecRunner.html
echo         ^<title^>Jasmine Spec Runner v3.4.0^</title^> >> SpecRunner.html  
echo. >> SpecRunner.html
echo         ^<!-- include source files here... --^> >> SpecRunner.html
echo. >> SpecRunner.html
echo         ^<!-- include spec files here... --^> >> SpecRunner.html
echo. >> SpecRunner.html
echo     ^</head^> >> SpecRunner.html  
echo     ^<body^ class="margin-0"^> >> SpecRunner.html  
echo     ^</body^> >> SpecRunner.html 
echo ^</html^> >> SpecRunner.html 

REM Criando a pasta para camada de teste
mkdir spec 

REM Install dependencies
echo 06 - Instaling webpack tools stack...
CALL npm i webpack webpack-cli webpack-dev-server html-webpack-plugin --D

REM Install jasmine-core
echo 07 - Instaling jasmine-core...
CALL npm i jasmine-core

echo Congratulations! 
echo Your web project was successfully created !!!
PAUSE 
