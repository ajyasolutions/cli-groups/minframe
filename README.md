# MinFrame

Um projeto de automação na criação de ambiente de desenvolvimento web. 
Protótipo na criação de um Framework web.

## Tecnologias utilizadas

* Script .bat

### O script gera um projeto web com a seguinte stack de tecnologis:

* Html
* CSS
* Javascript
* Webpack
* PWA
 
## Plataforma

Disponível somente para Windows.

## URL do projeto

https://min-frame.web.app/
